This code pulls current weather, every three hours, from across the UK.

It uses the library package data-point python (`pip install datapoint`),
which uses the MetOffice datapoint API.

https://github.com/jacobtomlinson/datapoint-python
https://www.metoffice.gov.uk/datapoint

The python code prints output to standard output (e.g. screen).

The bash code redirects output to file and pushes data up to GitLab.
The bash code contains an option to remove git history with each update.

The code can be schedules to run using `crontab - e` in Linux.
Use from crontab requires all file paths to be full absolute paths.

(The code will run from a Rasperry Pi Zero running Raspbian)
