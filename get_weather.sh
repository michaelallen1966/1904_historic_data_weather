#!/bin/bash

python3 /home/pi/Weather/get_weather_from_city_list.py >> /home/pi/Weather/weather_log.csv

git -C /home/pi/Weather add weather_log.txt
git -C /home/pi/Weather commit -m "Add latest weather to weather log"
git -C /home/pi/Weather push

# Optional code to clear git history

# Checkout

git -C /home/pi/Weather checkout --orphan latest_branch

# Add all the files

git -C /home/pi/Weather add -A

# commit the changes

git -C /home/pi/Weather commit -am "rewrite git"

# Delete the branch

git -C /home/pi/Weather branch -D master

# Rename the current branch to master

git -C /home/pi/Weather branch -m master

# Finally, force update your repository 

git -C /home/pi/Weather push -f origin master
